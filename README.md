# 🚀 Desafio front-end

O objetivo deste desafio é avaliar suas habilidades de programação.
Quando sua solução estiver pronta, basta responder o e-mail que recebeu com o link do seu repo aqui no Github!
Em seguida, enviaremos o feedback e as instruções dos próximos passos!

Caso tenha alguma dúvida, nós estamos disponíveis no email *work@dmxdesign.com.br*
Bom desafio!

> ⚠️ **É importante que o seu repo esteja público, caso contrário não iremos conseguir avaliar sua resposta**

# 🧠 Contexto

![Protótipo](https://storage.googleapis.com/xesque-dev/challenge-images/prototipo.png)

O desafio será implementar um e-commerce para venda de canecas e camisetas que deverá ter as seguintes funcionalidades:
- [ ] Catálogo de produtos com paginação
- [ ] Filtragem produtos por categoria
- [ ] Busca por nome do produto
- [ ] Adicionar e remover produtos do carrinho
- [ ] Finalizar compra

## 📋 Instruções

Chegou a hora de colocar a mão na massa!

- Siga [esse protótipo](https://www.figma.com/file/Cu09rrZIUSbydLrn0Yh2ev/E-commerce---capputeeno?type=design&node-id=680%3A6449&t=17HBUKECFoIITyyR-1)
- Utilize Next.js e Typescript
- Faça a estilização com styled-components
- Utilize o local storage para gerenciar o carrinho
- Por favor, inclua no README as instruções de instalação do projeto
- Sinta-se livre para incluir quaisquer observações
- Consuma nossa **API GraphQL** rodando o comando `yarn start` na pasta `api`

## ✔️ Critérios de Avaliação

Além dos requisitos levantados acima, iremos olhar para os seguintes critérios durante a correção do desafio:

- Gerenciamento de estado
- Componentização
- Responsividade
- Preocupação com usabilidade
- Preocupação com acessibilidade
- Testes e2e
- Padrões de código
- Padrão de commits (_Conventional_)

## 😎 Seria legal
- Fazer deploy na vercel ou em outro local de sua preferência e disponibilizar um link de visualização.
- Testes unitários
- Utilização de cache do Next.js

---

_O desafio acima foi cuidadosamente construído para propósitos de avaliação apenas. Já possuimos uma funcionalidade similar na nossa plataforma._
